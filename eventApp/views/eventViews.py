from rest_framework import views, status
from rest_framework.response import Response
from eventApp.models import Event
from eventApp.serializers import EventSerializer
from rest_framework import generics


class EventView(views.APIView):
    
    def get(self, request):
        queryset = Event.objects.all()
        serialized = EventSerializer(queryset, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


#List all accounts and Create an account
class EventListCreateView(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

# Retrieve, Update, Delete an account
class EventRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer