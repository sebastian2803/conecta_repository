from rest_framework import serializers
from eventApp.models.event import Event

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['id', 'title', 'initialDate', 'finalDate', 'elderlyId', 'description']