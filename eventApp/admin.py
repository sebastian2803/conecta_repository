from django.contrib import admin
from .models.event import Event

# Register your models here.

class EventAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'title', 'initialDate', 'finalDate', 'elderlyId', 'description')
    search_fields = ['title', 'id']
    list_filter = ('initialDate',)


admin.site.register(Event, EventAdmin)