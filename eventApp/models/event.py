from django.db import models
from authApp.models import ElderlyAccount

class Event(models.Model):
    id =  models.AutoField(primary_key=True)
    title = models.CharField(max_length=30)
    initialDate = models.DateTimeField(auto_now=False, auto_now_add=False)
    finalDate = models.DateTimeField(auto_now=False, auto_now_add=False)
    elderlyId = models.ForeignKey(
        ElderlyAccount,
        related_name="events_list",
        on_delete = models.CASCADE,
        null=True
        )
    description = models.TextField(null=True)