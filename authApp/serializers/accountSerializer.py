from rest_framework import serializers
from authApp.models import ElderlyAccount

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElderlyAccount
        fields = ['id', 'addres', 'phone', 'name', 'email', 'city']
