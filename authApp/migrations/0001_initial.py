# Generated by Django 3.2.7 on 2021-10-10 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ElderlyAccount',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('addres', models.CharField(max_length=25)),
                ('phone', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=25)),
                ('email', models.CharField(max_length=50)),
                ('city', models.CharField(max_length=25)),
            ],
        ),
    ]
