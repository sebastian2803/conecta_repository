from django.db import models

class ElderlyAccount(models.Model):
    id =  models.AutoField(primary_key=True)
    addres = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    description = models.TextField(null=True)


# /register(create)
# RESPONSE: elderlyId
