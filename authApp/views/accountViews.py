from rest_framework import views, status
from rest_framework.response import Response
from authApp.models import ElderlyAccount
from authApp.serializers import AccountSerializer
from rest_framework import generics

class AccountView(views.APIView):
    
    def get(self, request):
        queryset = ElderlyAccount.objects.all()
        serialized = AccountSerializer(queryset, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


#List all accounts and Create an account
class AccountListCreateView(generics.ListCreateAPIView):
    queryset = ElderlyAccount.objects.all()
    serializer_class = AccountSerializer

# Retrieve, Update, Delete an account
class AccountRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ElderlyAccount.objects.all()
    serializer_class = AccountSerializer


# /elderlyDelete(read(elderlyId))
# name: string
# phone: string
# address: string
# email: string
# city: string
# necessities: []necessity
# events: []event


# /elderlyList(read)
# name: string
# phone: string
# address: string
# email: string
# city: string
# id: int
