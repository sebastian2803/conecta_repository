from django.contrib import admin
from .models import ElderlyAccount

# Register your models here.

class AccountAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'addres', 'phone', 'name', 'email', 'city')
    search_fields = ['name', 'id']
    list_filter = ('id', 'name')



admin.site.register(ElderlyAccount, AccountAdmin)