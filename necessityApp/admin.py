from django.contrib import admin
from .models.necessity import Necessity

# Register your models here.

class NecessityAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'title', 'elderlyId', 'description')
    search_fields = ['title', 'elderlyId']
    list_filter = ('id',)


admin.site.register(Necessity, NecessityAdmin)
