from django.apps import AppConfig


class NecessityappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'necessityApp'
