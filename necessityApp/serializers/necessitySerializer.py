from rest_framework import serializers
from authApp.models.elderlyAccount import ElderlyAccount

class NecessitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ElderlyAccount
        fields = ['id', 'title', 'elderlyId', 'description']