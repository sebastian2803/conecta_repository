from django.db import models
from authApp.models import ElderlyAccount

class Necessity(models.Model):
    id =  models.AutoField(primary_key=True)
    title = models.CharField(max_length=30)
    elderlyId = models.ForeignKey(
        ElderlyAccount,
        related_name="necessities_list",
        on_delete = models.CASCADE,
        null=True
        )
    description = models.TextField(null=True)