from rest_framework import views, status
from rest_framework.response import Response
from necessityApp.models import Necessity
from necessityApp.serializers import NecessitySerializer
from rest_framework import generics


class NecessityView(views.APIView):
    
    def get(self, request):
        queryset = Necessity.objects.all()
        serialized = NecessitySerializer(queryset, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


#List all accounts and Create an account
class NecessityListCreateView(generics.ListCreateAPIView):
    queryset = Necessity.objects.all()
    serializer_class = NecessitySerializer

# Retrieve, Update, Delete an account
class NecessityRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Necessity.objects.all()
    serializer_class = NecessitySerializer